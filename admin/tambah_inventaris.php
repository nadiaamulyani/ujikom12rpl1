<?php
include "header.php";
?>

<!-- start: Content -->
          <div id="content">
              <div class="panel box-shadow-none content-header">
                <div class="panel-body">
                  <div class="col-md-12">
                      <h3 class="animated fadeInLeft">Inventaris</h3>
                      <p class="animated fadeInDown">
                        Inventaris <span class="fa-angle-right fa"></span> Tambah Data Inventaris
                      </p>
                  </div>
                </div>
              </div>
              <div class="form-element">
                <div class="panel form-element-padding">
                  <div class="panel-heading">
                    <h4>Tambah Data Inventaris</h4>
                  </div>
                    <form action="proses_simpan_inventaris.php" class="panel-body" style="padding-bottom:30px;" method="POST">
                      <div class="col-md-12">

                        <div class="form-group">
                          <label>Nama</label>
                            <input class="form-control" name="nama" type="text" placeholder="Masukan Nama" required>
                        </div>

						            <div class="form-group">
                          <label class="col-sm-2 control-label text-left">Kondisi</label>
                            <select class="form-control" name="kondisi">
                              <option value="" disabled seleted>Pilih Kondisi</option>
                              <option value="Baru">Baru</option>
							                <option value="Baik">Baik</option>
							                <option value="Rusak">Rusak</option>
                            </select>
                        </div>

                        <div class="form-group">
                          <label>Spesifikasi</label>
                            <input class="form-control" name="spesifikasi" type="text" placeholder="Tuliskan Spesifikasi" required>
                        </div>

						            <div class="form-group">
                          <label class="col-sm-2 control-label text-left">Keterangan</label>
                            <select class="form-control" name="keterangan_inventaris">
                              <option value="" disabled seleted>Pilih Keterangan</option>
                              <option value="Tersedia">Tersedia</option>
							                <option value="Dipinjam">Dipinjam</option>
                            </select>
                        </div>

                        <div class="form-group">
                          <label>Jumlah</label>
                            <input class="form-control" name="jumlah" type="text" value="1" readonly>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-2 control-label text-left">Nama Jenis</label>
                          <select class="form-control" name="id_jenis">
                            <option value="" disabled seleted>Pilih Nama Jenis</option>
                              <?php
                                include "../koneksi.php";
                                  $query = mysqli_query($conn, "SELECT * FROM jenis");
                                  while($data=mysqli_fetch_array($query)) {
                                ?>
                            <option value="<?php echo $data['id_jenis']; ?>"><?php echo $data['nama_jenis']; ?></option>
                              <?php } ?>
                          </select>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-2 control-label text-left">Nama Ruang</label>
                            <select class="form-control" name="id_ruang">
                              <option value="" disabled seleted>Pilih Nama Ruang</option>
                                <?php
                                  include "../koneksi.php";
                                    $query = mysqli_query($conn, "SELECT * FROM ruang");
                                    while($data=mysqli_fetch_array($query)) {
                                ?>
                              <option value="<?php echo $data['id_ruang']; ?>"><?php echo $data['nama_ruang']; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                          <label>Kode Inventaris</label>
                            <?php
                              $conn = mysqli_connect("localhost","root","","ujikom");
                              $cari_kd =mysqli_query($conn, "select max(kode_inventaris) as kode from inventaris");
                                //besar atau kode yang baru masuk
                              $tm_cari = mysqli_fetch_array($cari_kd);
                              $kode = substr($tm_cari['kode'],1,4);
                              $tambah = $kode+1;
                                if ($tambah<10){
                                $kode_inventaris ="V000".$tambah;
                                }else{
                                $kode_inventaris="V00".$tambah;
                                }
                            ?>
                            <input class="form-control" name="kode_inventaris" value="<?php echo $kode_inventaris; ?>" type="text" placeholder="Masukan Kode Inventaris" required>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-2 control-label text-left">Nama Petugas</label>
                            <select class="form-control" name="id_petugas">
                              <option value="" disabled seleted>Pilih Nama Petugas</option>
                                <?php
                                  include "../koneksi.php";
                                    $query = mysqli_query($conn, "SELECT * FROM petugas");
                                    while($data=mysqli_fetch_array($query)) {
                                ?>
                              <option value="<?php echo $data['id_petugas']; ?>"><?php echo $data['nama_petugas']; ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                          <label>Sumber</label>
                            <input class="form-control" name="sumber" type="text" placeholder="Masukan Sumber" required>
                        </div>

                      </div>
                      <button type="submit" name="submit" class="btn btn-3d btn-primary" value="submit" >Simpan</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
<!-- end: content -->

<?php
include "footer.php";
?>