<?php
include "header.php";
?>

<!-- start: Content -->
          <div id="content">
              <div class="panel box-shadow-none content-header">
                <div class="panel-body">
                  <div class="col-md-12">
                      <h3 class="animated fadeInLeft">Inventaris</h3>
                      <p class="animated fadeInDown">
                        Inventaris <span class="fa-angle-right fa"></span> Tambah Data Petugas
                      </p>
                  </div>
                </div>
              </div>
              <form method="post" action="proses_simpan_petugas.php">
              <div class="form-element">
                <div class="panel form-element-padding">
                  <div class="panel-heading">
                    <div class="panel-body" style="padding-bottom:30px;">
                      <div class="col-md-12">

                        <div class="form-group">
                          <label>Username</label>
                            <input class="form-control" name="username" type="text" placeholder="Masukan Username" required>
                        </div>

                        <div class="form-group">
                          <label>Email</label>
                            <input class="form-control" name="email" type="text" placeholder="Masukan Email" required>
                        </div>

                        <div class="form-group">
                          <label>Password</label>
                            <input class="form-control" name="password" type="text" placeholder="Tuliskan Password" required>
                        </div>

                        <div class="form-group">
                          <label>Nama Petugas</label>
                            <input class="form-control" name="nama_petugas" type="text" placeholder="Tuliskan Nama Petugas" required>
                        </div>

                        <div class="form-group">
                          <label class="col-sm-2 control-label text-left">Id Level</label>
                          <select class="form-control" name="id_level">
                            <option value="" disabled seleted>Pilih Id Level</option>
                              <?php
                                include "../koneksi.php";
                                  $query = mysqli_query($conn, "SELECT * FROM level");
                                  while($data=mysqli_fetch_array($query)) {
                                ?>
                            <option name="id_level"  value="<?php echo $data['id_level']; ?>"><?php echo $data['nama_level']; ?></option>
                              <?php } ?>
                          </select>
                        </div>

                      </div>
                      <button type="submit" name="submit" class="btn btn-3d btn-primary" value="submit" >Simpan</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
                
<!-- end: content -->

<?php
include "footer.php";
?>