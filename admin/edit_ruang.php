<?php
	include "header.php";
?>

<!-- start: Content -->
            <div id="content">
                <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft">From Edit Data Ruang</h3>
                        <p class="animated fadeInDown">
                          Inventaris <span class="fa-angle-right fa"></span> Edit Data Ruang
                        </p>
                    </div>
                  </div>
                </div>

<?php 
  include "../koneksi.php";
  $id_ruang = $_GET['id_ruang'];
  $query_mysqli = mysqli_query($conn, "SELECT * FROM ruang WHERE id_ruang='$id_ruang'")or die(mysqli_error());
  while($data = mysqli_fetch_array($query_mysqli)){
?>

<form action="proses_edit_ruang.php" method="post">
                <div class="form-element">
                      <div class="panel form-element-padding">
                        <div class="panel-heading">
                         <h4>Edit Data Ruang</h4>
                        </div>
                         <div class="panel-body" style="padding-bottom:30px;">
                          <div class="col-md-24">
                          <div class="form-group">
							<label>Id Ruang</label>
							<input class="form-control" name="id_ruang" type="text" placeholder="Id Ruang" value="<?=$data['id_ruang']?>" required>
							</div>

							<div class="form-group">
							<label>Nama Ruang</label>
							<input class="form-control" name="nama_ruang" type="text" placeholder="Masukan Nama Ruang" value="<?=$data['nama_ruang']?>" required>
							</div>

							<div class="form-group">
							<label>Jenis Ruang</label>
							<input class="form-control" name="jenis_ruang" type="text" placeholder="Masukan Jenis Ruang" value="<?=$data['jenis_ruang']?>" required>
							</div>

							<div class="form-group">
							<label>Keterangan</label>
							<input class="form-control" name="keterangan" type="text" placeholder="Masukan Keterangan" value="<?=$data['keterangan']?>" required>
							</div>

                        <div class="col-md-6">
                            <button type="submit" class="btn btn-3d btn-primary">Simpan</button>
                        </div>
                        </div>
                        </form>
                      </div>
                    </div>

<?php
  }
?>
<!-- end: content -->

<?php
	include "footer.php";
?>