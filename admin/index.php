<?php
	include "header.php";
?>

  		
          <!-- start: content -->
            <div id="content">
                <div class="panel">
                  <div class="panel-body">
                      <div class="col-md-6 col-sm-12">
                        <h3 class="animated fadeInLeft">Data Barang Perjurusan</h3>
                        <p class="animated fadeInDown"><span class="fa  fa-map-marker"></span> Bogor,Indonesia</p>

                        <ul class="nav navbar-nav">
                            <li><a href="data_inventaris.php" >Inventaris</a></li>
                            <li><a href="data_jenis.php" >Jenis</a></li>
                            <li><a href="data_ruang.php">Ruang</a></li>
                            <li><a href="data_petugas.php">Petugas</a></li>
                        </ul>

                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="col-md-6 col-sm-6 text-right" style="padding-left:10px;">
                          <h3 style="color:#DDDDDE;"><span class="fa  fa-map-marker"></span> Bogor</h3>
                          <h1 style="margin-top: -10px;color: #ddd;">30<sup>o</sup></h1>
                        </div>
                        <div class="col-md-6 col-sm-6">
                           <div class="wheather">
                            <div class="stormy rainy animated pulse infinite">
                              <div class="shadow">
                                
                              </div>
                            </div>
                            <div class="sub-wheather">
                              <div class="thunder">
                                
                              </div>
                              <div class="rain">
                                  <div class="droplet droplet1"></div>
                                  <div class="droplet droplet2"></div>
                                  <div class="droplet droplet3"></div>
                                  <div class="droplet droplet4"></div>
                                  <div class="droplet droplet5"></div>
                                  <div class="droplet droplet6"></div>
                                </div>
                            </div>
                          </div>
                        </div>                   
                    </div>
                  </div>                    
                </div>

                <div class="col-md-12" style="padding:20px;">
                    <div class="col-md-12 padding-0"> 
							
							                    <div class="col-md-4">
                                    <div class="panel box-v1">
                                      <div class="panel-heading bg-white border-none">
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
											                    <h4>Data RPL</h4>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                           <h4>
                                           <span class="icon-basket-loaded icons icon text-right"></span>
                                           </h4>
                                        </div>
                                      </div>
                                      <div class="panel-body text-center">
                                        <a href="data_inventaris.php?jurusan=RPL"><h1>RPL</h1>
                                        <p>Rekayasa Perangkat Lunak</p>
                                        <hr/>
                                      </div>
                                    </div>
                                  </div>
								
								                  <div class="col-md-4">
                                    <div class="panel box-v1">
                                      <div class="panel-heading bg-white border-none">
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
											                    <h4>Data ANM</h4>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                           <h4>
                                           <span class="icon-basket-loaded icons icon text-right"></span>
                                           </h4>
                                        </div>
                                      </div>
                                      <div class="panel-body text-center">
                                        <a href="data_inventaris.php?jurusan=ANM"><h1>ANIMASI</h1>
                                        <p>Animasi</p>
                                        <hr/>
                                      </div>
                                    </div>
                                  </div>
								
								                  <div class="col-md-4">
                                    <div class="panel box-v1">
                                      <div class="panel-heading bg-white border-none">
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
											                    <h4>Data TKR</h4>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                           <h4>
                                           <span class="icon-basket-loaded icons icon text-right"></span>
                                           </h4>
                                        </div>
                                      </div>
                                      <div class="panel-body text-center">
                                        <a href="data_inventaris.php?jurusan=TKR"><h1>TKR</h1>
                                        <p>Teknik Kendaraan Ringan</p>
                                        <hr/>
                                      </div>
                                    </div>
                                  </div>
								
								                  <div class="col-md-4">
                                    <div class="panel box-v1">
                                      <div class="panel-heading bg-white border-none">
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
											                    <h4>Data TPL</h4>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                           <h4>
                                           <span class="icon-basket-loaded icons icon text-right"></span>
                                           </h4>
                                        </div>
                                      </div>
                                      <div class="panel-body text-center">
                                        <a href="data_inventaris.php?jurusan=TPL"><h1>TPL</h1>
                                        <p>Teknik Pengelasan</p>
                                        <hr/>
                                      </div>
                                    </div>
                                  </div>
								
								                  <div class="col-md-4">
                                    <div class="panel box-v1">
                                      <div class="panel-heading bg-white border-none">
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-left padding-0">
											                    <h4>Data Broadcasting</h4>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                           <h4>
                                           <span class="icon-basket-loaded icons icon text-right"></span>
                                           </h4>
                                        </div>
                                      </div>
                                      <div class="panel-body text-center">
                                        <a href="data_inventaris.php?jurusan=BC"><h1>Broadcasting</h1>
                                        <p>Broadcasting</p>
                                        <hr/>
                                      </div>
                                    </div>
                                  </div>
								                </div>
                              </div>
					                  </div>
				                  </div>
          <!-- end: content -->

    
          <?php
	include "footer.php";
?>