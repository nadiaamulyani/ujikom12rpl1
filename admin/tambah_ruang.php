<?php
include "header.php";
?>

<!-- start: Content -->
          <div id="content">
              <div class="panel box-shadow-none content-header">
                <div class="panel-body">
                  <div class="col-md-12">
                      <h3 class="animated fadeInLeft">Inventaris</h3>
                      <p class="animated fadeInDown">
                        Inventaris <span class="fa-angle-right fa"></span> Tambah Data Ruang
                      </p>
                  </div>
                </div>
              </div>
              <form method="post" action="proses_simpan_ruang.php">
              <div class="form-element">
                <div class="panel form-element-padding">
                  <div class="panel-heading">
                    <div class="panel-body" style="padding-bottom:30px;">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label>Nama Ruang</label>
                            <input class="form-control" name="nama_ruang" type="text" placeholder="Masukan Nama Ruang" required>
                        </div>

                        <div class="form-group">
                          <label>Jenis Ruang</label>
                            <input class="form-control" name="jenis_ruang" type="text" placeholder="Masukan Jenis Ruang" required>
                        </div>

                        <div class="form-group">
                          <label>Keterangan</label>
                            <input class="form-control" name="keterangan" type="text" placeholder="Tuliskan Keterangan" required>
                        </div>
                      </div>
                        <button type="submit" name="submit" class="btn btn-3d btn-primary" value="submit" >Simpan</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
                
<!-- end: content -->

<?php
include "footer.php";
?>