<?php
  include "header.php";
?>
 <!-- start: Content -->
 <div id="content">
 <div class="panel box-shadow-none content-header">
    <div class="panel-body">
      <div class="col-md-12">
          <h3 class="animated fadeInLeft">Data Inventarisir</h3>
          <p class="animated fadeInDown">
            Inventarisir <span class="fa-angle-right fa"></span> Data Jenis
          </p>
      </div>
    </div> 
    </div>

<!-- MODAL -->
<div id="exportpdf" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap Data Inventaris PDF</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="pdf_inven_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="pdf_inventaris.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
<!-- EXCEL INVENTARIS -->
<div id="exportexcel" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap Data Inventaris Excel</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="excel_inven_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="export_excel_inventaris.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>

<!-- EXCEL ALL DATA PEMINJAMAN -->
<div id="export_excel_pinkem" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap ALL DATA PEMINJAMAN Excel</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="excelall_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="export_all.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
<!-- EXCEL ALL DATA PEMINJAMAN -->
<div id="export_pdf_pinkem" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap ALL DATA PEMINJAMAN PDF</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="pdfall_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="pdf_all.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>

<div id="export_excel_kembali" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA PENGEMBALIAN Excel</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="excelkem_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="export_kembali.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
<!-- /.MODAL -->
<div id="export_pdf_kembali" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA PENGEMBALIAN PDF</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="pdfkem_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="pdf_kembali.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
<!-- /.MODAL -->

<!-- /.MODAL -->
<div id="export_pdf_pinjam" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA PEMIMJAMAN PDF</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="pdfpinjam_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="pdf_pinjam.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
<!-- /.MODAL -->

<!-- /.MODAL -->
<div id="export_excel_pinjam" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<h4 class="modal-title">Rekap DATA PEMIMJAMAN EXCEL</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>

</div>
<div class="modal-body">
<form action="excelpinjam_tgl.php" method="post" target="_blank">
<table>
    <tr>
        <td>
            <div class="form-group">Dari Tanggal</div>
        </td>
        <td align="center" width="5%">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_a" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="form-group">Sampai Tanggal</div>
        </td>
        <td align="center">
            <div class="form-group">:</div>
        </td>
        <td>
           <div class="form-group">
               <input type="date" class="form-control" name="tgl_b" required>
           </div> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td>
            <input type="submit" name="export_barang" class="btn btn-info btn-sm" value="Export">
        </td>
    </tr>
</table>
</form>
</div>
<div class="modal-footer">
<a href="export_pinjam.php" target="_blank" class="btn btn-info btn-sm" >Export Semua</a>
</div>
</div>
</div>
</div>
<!-- /.MODAL -->

<div class="col-md-12 top-20 padding-0">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading"><h3>Data Jenis</h3></div>
      <div class="panel-body">
        <div class="responsive-table">
        <table id="datatables-example" class="table table-striped table-bordered" cellspacing="0">
        <thead>
          <tr>
            <th>Nama</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
            <tr>
                <td>Inventaris</td>
                <td>
                    <div class="col-md-6">
                        <a data-toggle="modal" data-target="#exportexcel" type="button" class="btn btn-3d btn-success">Excel</a>
                        <a data-toggle="modal" data-target="#exportpdf"class="btn btn-3d btn-default">PDF</a>
                    </div>
                </td>
            </tr>
            
            <tr>
                <td>Data Pengembalian</td>
                <td>
                    <div class="col-md-6">
                        <a data-toggle="modal" data-target="#export_excel_kembali" class="btn btn-3d btn-success">Excel</a>
                        <a data-toggle="modal" data-target="#export_pdf_kembali" class="btn btn-3d btn-default">PDF</a>
                    </div>
                </td>
            </tr>

            <tr>
                <td>Data Peminjaman</td>
                <td>
                    <div class="col-md-6">
                        <a data-toggle="modal" data-target="#export_excel_pinjam" class="btn btn-3d btn-success">Excel</a>
                        <a data-toggle="modal" data-target="#export_pdf_pinjam" class="btn btn-3d btn-default">PDF</a>
                    </div>
                </td>
            </tr>

            <tr>
                <td>Semua Data</td>
                <td>
                    <div class="col-md-6">
                        <a data-toggle="modal" data-target="#export_excel_pinkem" class="btn btn-3d btn-success">Excel</a>
                        <a data-toggle="modal" data-target="#export_pdf_pinkem" class="btn btn-3d btn-default">PDF</a>
                    </div>
                </td>
            </tr>
		</tbody>
    </table>
</div>
</div> 
</div>
</div>
</div>  
</div>
</div>
<!-- end: content -->

<?php
  include "footer.php";
?>