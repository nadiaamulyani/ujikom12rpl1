<?php
$connect = new PDO("mysql:host=localhost;dbname=ujikom", "root", "");
$get_all_table_query = "SHOW TABLES";
$statement = $connect->prepare($get_all_table_query);
$statement->execute();
$result = $statement->fetchAll();

if(isset($_POST['table']))
{
 $output = '';
 foreach($_POST["table"] as $table)
 {
  $show_table_query = "SHOW CREATE TABLE " . $table . "";
  $statement = $connect->prepare($show_table_query);
  $statement->execute();
  $show_table_result = $statement->fetchAll();

  foreach($show_table_result as $show_table_row)
  {
   $output .= "\n\n" . $show_table_row["Create Table"] . ";\n\n";
  }
  $select_query = "SELECT * FROM " . $table . "";
  $statement = $connect->prepare($select_query);
  $statement->execute();
  $total_row = $statement->rowCount();

  for($count=0; $count<$total_row; $count++)
  {
   $single_result = $statement->fetch(PDO::FETCH_ASSOC);
   $table_column_array = array_keys($single_result);
   $table_value_array = array_values($single_result);
   $output .= "\nINSERT INTO $table (";
   $output .= "" . implode(", ", $table_column_array) . ") VALUES (";
   $output .= "'" . implode("','", $table_value_array) . "');\n";
  }
 }
 $file_name = 'inventaris_smk_' . date('y-m-d') . '.sql';
 $file_handle = fopen($file_name, 'w+');
 fwrite($file_handle, $output);
 fclose($file_handle);
 header('Content-Description: File Transfer');
 header('Content-Type: application/octet-stream');
 header('Content-Disposition: attachment; filename=' . basename($file_name));
 header('Content-Transfer-Encoding: binary');
 header('Expires: 0');
 header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file_name));
    ob_clean();
    flush();
    readfile($file_name);
    unlink($file_name);
}

?>


<?php
	include "header.php";
?>

          <!-- start: Content -->
          <div id="content">
            <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft">Backup Database</h3>
                        <p class="animated fadeInDown">
                         Backup Database<span class="fa-angle-right fa"></span> Backup Database
                        </p>
                    </div>
                  </div>
              </div>
			  
            <div class="col-md-12 modal-example">
                 <div class="col-md-12">
                    <div class="panel">
                      <div class="panel-heading">
                        <h4>Backup Database</h4>
                      </div>
                      <div class="panel-body">
                            
                            <div class="col-md-12">
                                <div class="modal fade modal-v1">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h2 class="modal-title">
                                          <i class="icon-user icons"></i>
                                        </h2>
                                      </div>
                                      <div class="modal-body">
                                        <h3>Backup Database </h3>
                                        <p>Data kamu sudah penuh!! ini saat nya kamu untuk backup database.</p>
                                        <div class="panel-body">
                                        <?php
                                          error_reporting(0);
                                          $file=date("Ymd").'_backup_database_'.time().'.sql';
                                          backup_tables("localhost","root","","ujikom",$file);
                                        ?>
                                            <div class="form-group pull-right">
                                              <a style="cursor:pointer" onclick="location.href='download_backup_data.php?nama_file=<?php echo $file;?>'" title="Download" class="icon-layers" >&nbsp;Backup</a>
                                            </div> 
                                            <?php
                        /*
                        untuk memanggil nama fungsi :: jika anda ingin membackup semua tabel yang ada didalam database, biarkan tanda BINTANG (*) pada variabel $tables = '*'
                        jika hanya tabel-table tertentu, masukan nama table dipisahkan dengan tanda KOMA (,) 
                        */
                        function backup_tables($host,$user,$pass,$name,$nama_file,$tables ='*') {
                          $link = mysql_connect($host,$user,$pass);
                          mysql_select_db($name,$link);
                          
                          if($tables == '*'){
                            $tables = array();
                            $result = mysql_query('SHOW TABLES');
                            while($row = mysql_fetch_row($result)){
                              $tables[] = $row[0];
                            }
                          }
                          else{//jika hanya table-table tertentu
                            $tables = is_array($tables) ? $tables : explode(',',$tables);
                          }
                          
                          foreach($tables as $table){
                            $result = mysql_query('SELECT * FROM '.$table);
                            $num_fields = mysql_num_fields($result);
                                            
                            $return.= 'DROP TABLE '.$table.';';//menyisipkan query drop table untuk nanti hapus table yang lama
                            $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
                            $return.= "\n\n".$row2[1].";\n\n";
                            
                            for ($i = 0; $i < $num_fields; $i++) {
                              while($row = mysql_fetch_row($result)){
                                //menyisipkan query Insert. untuk nanti memasukan data yang lama ketable yang baru dibuat
                                $return.= 'INSERT INTO '.$table.' VALUES(';
                                for($j=0; $j<$num_fields; $j++) {
                                  //akan menelusuri setiap baris query didalam
                                  $row[$j] = addslashes($row[$j]);
                                  $row[$j] = ereg_replace("\n","\\n",$row[$j]);
                                  if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                                  if ($j<($num_fields-1)) { $return.= ','; }
                                }
                                $return.= ");\n";
                              }
                            }
                            $return.="\n\n\n";
                          }             
                          //simpan file di folder
                          $nama_file;
                          
                          $handle = fopen('backup/'.$nama_file,'w+');
                          fwrite($handle,$return);
                          fclose($handle);
                        }
                        ?>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                      </div>
                                    </div><!-- /.modal-content -->
                                  </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            </div>

                      </div>
                    </div>
                </div>
            </div>
			
			<div class="col-md-12 modal-example">
                 <div class="col-md-12">
                    <div class="panel">
                      <div class="panel-heading">
                        <h4>Backup Database</h4>
                      </div>
                      <div class="panel-body">
                            
                            <div class="col-md-12">
							<br>
							<br>
                                <div class="modal fade modal-v1">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h2 class="modal-title">
                                          <i class="icon-user icons"></i>
                                        </h2>
                                      </div>
									  <center>
                                        <div class="panel-body">
                                        
											<form method="post" id="export_form" >
												<h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pilih Tabel Untuk Export</h3>
	<?php
		foreach($result as $table)
	{
	?>
											
											<div class="checkbox">
												<label><input type="checkbox" class="checkbox_table" name="table[]" value="<?php echo $table["Tables_in_ujikom"]; ?>" /> <?php echo $table["Tables_in_ujikom"]; ?></label>
											</div>
    <?php
    }
    ?>
											<div class="form-group">
												<input type="submit" name="submit" id="submit" class="btn btn-primary" value="Export" />
											</div>
											</form>
										
                                        </div>
										</center>
                                      <div class="modal-footer">
                                      </div>
                                    </div><!-- /.modal-content -->
                                  </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            </div>

                      </div>
                    </div>
                </div>
            </div>
			
          </div>
          <!-- end: Content -->


<?php
	include "footer.php";
?>