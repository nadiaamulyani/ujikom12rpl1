<?php
	include "header.php";
?>

<!-- start: Content -->
            <div id="content">
                <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft">From Edit Data Jenis</h3>
                        <p class="animated fadeInDown">
                          Inventaris <span class="fa-angle-right fa"></span> Edit Data Jenis
                        </p>
                    </div>
                  </div>
                </div>

<?php 
  include "../koneksi.php";
  $id_jenis = $_GET['id_jenis'];
  $query_mysqli = mysqli_query($conn, "SELECT * FROM jenis WHERE id_jenis='$id_jenis'")or die(mysqli_error());
  while($data = mysqli_fetch_array($query_mysqli)){
?>

  <form action="proses_edit_jenis.php" method="post">
    <div class="form-element">
      <div class="panel form-element-padding">
        <div class="panel-heading">
          <h4>Edit Data Jenis</h4>
        </div>
          <div class="panel-body" style="padding-bottom:30px;">
            <div class="col-md-24">
              <div class="form-group">
							  <label>Id Jenis</label>
							  <input class="form-control" name="id_jenis" type="text" placeholder="Masukan Nama Jenis" value="<?=$data['id_jenis']?>" required>
							</div>

							<div class="form-group">
							  <label>Nama Jenis</label>
							  <input class="form-control" name="nama_jenis" type="text" placeholder="Masukan Nama Jenis" value="<?=$data['nama_jenis']?>" required>
							</div>

                        <div class="form-group">
                          <label>Kode Jenis</label>
                            <?php
                              $conn = mysqli_connect("localhost","root","","ujikom");
                              $cari_kd =mysqli_query($conn, "select max(kode_jenis) as kode from jenis");
                                //besar atau kode yang baru masuk
                              $tm_cari = mysqli_fetch_array($cari_kd);
                              $kode = substr($tm_cari['kode'],1,4);
                              $tambah = $kode+1;
                                if ($tambah<10){
                                $kode_jenis ="J000".$tambah;
                                }else{
                                $kode_jenis="J00".$tambah;
                                }
                            ?>
                            <input class="form-control" name="kode_jenis" value="<?php echo $kode_jenis; ?>" type="text" placeholder="Masukan Kode Inventaris" required>
                        </div>

							<div class="form-group">
							<label>Keterangan</label>
							<input class="form-control" name="keterangan" type="text" placeholder="Masukan Keterangan" value="<?=$data['keterangan']?>" required>
							</div>

              <div class="col-md-6">
                <button type="submit" class="btn btn-3d btn-primary">Simpan</button>
              </div>
            </div>
  </form>
</div>
</div>

<?php
  }
?>
<!-- end: content -->

<?php
	include "footer.php";
?>