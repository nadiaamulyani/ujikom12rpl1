<?php
  include "header.php";
?>
  <!-- start: Content -->
  <div id="content">
    <div class="panel box-shadow-none content-header">
      <div class="panel-body">
        <div class="col-md-12">
          <h3 class="animated fadeInLeft">Data Inventarisir</h3>
          <p class="animated fadeInDown">
            Inventarisir <span class="fa-angle-right fa"></span> Data Inventarisir
          </p>
        </div>
      </div>
    </div>
    <div class="col-md-15 top-15 padding-0">
      <div class="col-md-15">
        <div class="panel">
          <div class="panel-heading">
            <h3>Data Inventarisir</h3>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table id="datatables-example" class="table table-striped table-bordered" cellspacing="0">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Kondisi</th>
                    <th>Spesifikasi</th>
                    <th>Keterangan</th>
                    <th>Jumlah</th>
                    <th>Jenis</th>
                    <th>Tanggal Register</th>
                    <th>Ruang</th>
                    <th>Kode Inventaris</th>
                    <th>Petugas</th>
                    <th>Sumber</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
<?php // Load file koneksi.php
  include "../koneksi.php";
    if (isset($_GET['jurusan'])) {
      $bebas = $_GET['jurusan'];                    
      $query = "SELECT * FROM `inventaris` JOIN jenis ON inventaris.id_jenis=jenis.id_jenis JOIN ruang ON ruang.id_ruang=inventaris.id_ruang JOIN petugas ON petugas.id_petugas=inventaris.id_petugas WHERE jenis.nama_jenis = '$bebas' order by id_inventaris desc"; // Query untuk menampilkan semua data siswa
    } else {
      $query = "SELECT * FROM `inventaris` JOIN jenis ON inventaris.id_jenis=jenis.id_jenis JOIN ruang ON ruang.id_ruang=inventaris.id_ruang JOIN petugas ON petugas.id_petugas=inventaris.id_petugas order by id_inventaris desc";
    }
      $sql = mysqli_query($conn, $query); // Eksekusi/Jalankan query dari variabel $query
      $no=1;
     while($data = mysqli_fetch_array($sql)){
?>
                  <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $data['nama']; ?></td>
                    <td><?php echo $data['kondisi']; ?></td>
                    <td><?php echo $data['spesifikasi']; ?></td>
                    <td><?php echo $data['keterangan_inventaris']; ?></td>
                    <td><?php echo $data['jumlah']; ?></td>
                    <td><?php echo $data['nama_jenis']; ?></td>
                    <td><?php echo $data['tanggal_register']; ?></td>
                    <td><?php echo $data['nama_ruang']; ?></td>
                    <td><?php echo $data['kode_inventaris']; ?></td>
                    <td><?php echo $data['nama_petugas']; ?></td>
                    <td><?php echo $data['sumber']; ?></td>
                    <td>
                      <div class="col-md-6">
                        <a href="edit_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>" type="button" class="btn btn-3d btn-default">Edit</a>
                      </div>
                    </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
              <div>
                <a href="tambah_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>" type="button" class="btn btn-3d btn-primary">Added</a>
                <!-- <a href="pdf_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>" type="button" class="btn btn-3d btn-warning">Export PDF</a>
                <a href="export_excel_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>" type="button" class="btn btn-3d btn-success">Export Exel</a> -->
              </div>
              <br>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- end: content -->

    <?php
  include "footer.php";
?>