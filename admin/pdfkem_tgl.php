<?php
include '../koneksi.php';
require('../asset/pdf/fpdf.php');

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('../asset/img/skanic.png',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'SMK NEGERI 1 CIOMAS',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telp : (0251)8631261',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'JL. Raya Laladon Ds.Laladon, Kec.Ciomas Kab.Bogor Kode Pos. 16610',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Email : smkn1_ciomas@yahoo.co.id, Website : www.smkn1ciomas.sch.id',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data Inventaris",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Nama', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Tanggal Pinjam', 1, 0, 'C'); 
$pdf->Cell(4, 0.8, 'Tangga Kembali', 1, 0, 'C');
$pdf->Cell(2, 0.8, 'Status', 1, 1, 'C');  
$pdf->SetFont('Arial','',9);
$no=1;

            $tanggal_awal = $_POST['tgl_a'];
            $tanggal_akhir = $_POST['tgl_b'];
$query= mysqli_query($conn,"SELECT * from peminjaman INNER JOIN pegawai ON peminjaman.id_pegawai=pegawai.id_pegawai where status_peminjaman='dikembalikan'  and tanggal_pinjam between '$tanggal_awal' and '$tanggal_akhir' ORDER BY id_peminjaman DESC");
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no++ , 1, 0, 'C');
	$pdf->Cell(3, 0.8, $lihat['nama_pegawai'],1, 0, 'C');
	$pdf->Cell(4, 0.8, $lihat['tanggal_pinjam'], 1, 0,'C'); 
	$pdf->Cell(4, 0.8, $lihat['tanggal_kembali'], 1, 0,'C');
	$pdf->Cell(2, 0.8, $lihat['status_peminjaman'],1, 1, 'C'); 
	
}
	$pdf->Output("laporan_inventaris.pdf","I");

?>

