<?php
  include "header.php";
?>
 <!-- start: Content -->
 <div id="content">
 <div class="panel box-shadow-none content-header">
    <div class="panel-body">
      <div class="col-md-12">
          <h3 class="animated fadeInLeft">Data Inventarisir</h3>
          <p class="animated fadeInDown">
            Inventarisir <span class="fa-angle-right fa"></span> Data Ruang
          </p>
      </div>
    </div> 
    </div>
<div class="col-md-12 top-20 padding-0">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading"><h3>Data Ruang</h3></div>
      <div class="panel-body">
        <div class="responsive-table">
        <table id="datatables-example" class="table table-striped table-bordered" cellspacing="0">
        <thead>
          <tr>
            <th>#</th>
            <th>Nama Ruang</th>
            <th>Jenis Ruang</th>
            <th>Keterangan</th>
			      <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
<?php // Load file koneksi.php
                 include "../koneksi.php";

                  $query = "SELECT * FROM ruang order by id_ruang desc"; // Query untuk menampilkan semua data siswa
                  $sql = mysqli_query($conn, $query); // Eksekusi/Jalankan query dari variabel $query
                  $no=1;
                  while($data = mysqli_fetch_array($sql)){
                    ?> 
          <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo $data['nama_ruang']; ?></td>
            <td><?php echo $data['jenis_ruang']; ?></td>
            <td><?php echo $data['keterangan']; ?></td>
            <td>
          <div class="col-md-6">
              <a href="edit_ruang.php?id_ruang=<?php echo $data['id_ruang']; ?>" type="button" class="btn btn-3d btn-default">Edit</a>
          </div>
            </td>
          </tr>
          <?php } ?>
        </tbody>
          </table>
          </div> 
              <a href="tambah_ruang.php?id_ruang=<?php echo $data['id_ruang']; ?>" type="button" class="btn btn-3d btn-primary">Added</a>
              <a href="tambah_ruang.php?id_ruang=<?php echo $data['id_ruang']; ?>" type="button" class="btn btn-3d btn-warning">Export PDF</a>
              <a href="tambah_ruang.php?id_ruang=<?php echo $data['id_ruang']; ?>" type="button" class="btn btn-3d btn-success">Export Exel</a>
          </div> 
    </div>
  </div>
</div>  
</div>
</div>
<!-- end: content -->

<?php
  include "footer.php";
?>