<?php
	include "header.php";
?>

<!-- start: Content -->
            <div id="content">
                <div class="panel box-shadow-none content-header">
                  <div class="panel-body">
                    <div class="col-md-12">
                        <h3 class="animated fadeInLeft">From Edit Data Petugas</h3>
                        <p class="animated fadeInDown">
                          Inventaris <span class="fa-angle-right fa"></span> Edit Data Petugas
                        </p>
                    </div>
                  </div>
                </div>

<?php 
    include "../koneksi.php";
    $id_petugas = $_GET['id_petugas'];
    $query_mysqli = mysqli_query($conn, "SELECT * FROM petugas inner join level on petugas.id_petugas = level.id_level WHERE id_petugas='$id_petugas'")or die(mysqli_error());
    while($data = mysqli_fetch_array($query_mysqli)){
?>

<form action="proses_edit_petugas.php" method="post">
    <div class="form-element">
        <div class="panel form-element-padding">
            <div class="panel-heading">
                <h4>Edit Data Petugas</h4>
            </div>
                <div class="panel-body" style="padding-bottom:30px;">
                    <div class="col-md-24">

                        <div class="form-group" hidden="">
						    <label>Username</label>
						    <input class="form-control" name="id_petugas" type="text" placeholder="Masukan Username" value="<?=$data['id_petugas']?>" required>
						</div>

                        <div class="form-group">
						    <label>Username</label>
						    <input class="form-control" name="username" type="text" placeholder="Masukan Username" value="<?=$data['username']?>" required>
						</div>

						<div class="form-group">
						    <label>Email</label>
							<input class="form-control" name="email" type="text" placeholder="Masukan Email" value="<?=$data['email']?>" required>
						</div>

						<div class="form-group">
							<label>Password</label>
							<input class="form-control" name="password" type="text" placeholder="Masukan Password" value="<?=$data['password']?>" required>
						</div>

						<div class="form-group">
							<label>Nama Petugas</label>
							<input class="form-control" name="nama_petugas" type="text" placeholder="Masukan Nama Petugas" value="<?=$data['nama_petugas']?>" required>
						</div>

                        <div class="form-group">
							<label>Id Level</label>
							<input class="form-control" name="id_level" type="text" placeholder="Tuliskan id Level" value="<?=$data['id_level']?>" required>
						</div>
                        
					    <div class="form-group">
                            <label class="col-sm-2 control-label text-left">Baned</label>
                                <select class="form-control" name="baned">
                                    <option value="" disabled seleted>Pilih Baned</option>
                                    <option value="No">Tidak Terblokir</option>
							        <option value="Yes">Terblokir</option>
                                </select>
                        </div>

                        <div class="form-group">
							<label>Logintime</label>
							<input class="form-control" name="logintime" type="text" placeholder="Tuliskan Logintime" value="<?=$data['logintime']?>" required>
						</div>

                        <div class="col-md-6">
                            <button type="submit" class="btn btn-3d btn-primary">Simpan</button>
                        </div>

                     </div>
                </form>
            </div>
        </div>
<?php
  }
?>
<!-- end: content -->

<?php
	include "footer.php";
?>