<?php
  include "header.php";
?>
 <!-- start: Content -->
 <div id="content">
 <div class="panel box-shadow-none content-header">
    <div class="panel-body">
      <div class="col-md-12">
          <h3 class="animated fadeInLeft">Data Inventarisir</h3>
          <p class="animated fadeInDown">
            Inventarisir <span class="fa-angle-right fa"></span> Data Jenis
          </p>
      </div>
    </div> 
    </div>
<div class="col-md-12 top-20 padding-0">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading"><h3>Data Jenis</h3></div>
      <div class="panel-body">
        <div class="responsive-table">
        <table id="datatables-example" class="table table-striped table-bordered" cellspacing="0">
        <thead>
          <tr>
            <th>#</th>
            <th>Nama Jenis</th>
            <th>Kode Jenis</th>
            <th>Keterangan</th>
			      <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
<?php // Load file koneksi.php
  include "../koneksi.php";
    $query = "SELECT * FROM jenis order by id_jenis desc"; // Query untuk menampilkan semua data siswa
    $sql = mysqli_query($conn, $query); // Eksekusi/Jalankan query dari variabel $query
    $no=1;
  while($data = mysqli_fetch_array($sql)){
?> 
          <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo $data['nama_jenis']; ?></td>
            <td><?php echo $data['kode_jenis']; ?></td>
            <td><?php echo $data['keterangan']; ?></td>
            <td>
          <div class="col-md-6">
              <a href="edit_jenis.php?id_jenis=<?php echo $data['id_jenis']; ?>" type="button" class="btn btn-3d btn-default">Edit</a>
          </div>
            </td>
          </tr>
          <?php } ?>
        </tbody>
          </table>
          </div> 
              <a href="tambah_jenis.php?id_jenis=<?php echo $data['id_jenis']; ?>" type="button" class="btn btn-3d btn-primary">Added</a>
              <a href="export_pdf_jenis.php?id_jenis=<?php echo $data['id_jenis']; ?>" type="button" class="btn btn-3d btn-warning">Export PDF</a>
              <a href="tambah_jenis.php?id_jenis=<?php echo $data['id_jenis']; ?>" type="button" class="btn btn-3d btn-success">Export Exel</a>
          </div> 
    </div>
  </div>
</div>  
</div>
</div>
<!-- end: content -->

<?php
  include "footer.php";
?>