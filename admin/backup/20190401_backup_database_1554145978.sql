DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `jumlahp` varchar(5) NOT NULL,
  `status_peminjaman` enum('dipinjam','dikembalikan') NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `id_inventaris` (`id_inventaris`),
  KEY `id_peminjaman` (`id_peminjaman`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("1","1","1","dikembalikan","0");
INSERT INTO detail_pinjam VALUES("2","16","10","dikembalikan","50");
INSERT INTO detail_pinjam VALUES("3","16","1","dikembalikan","50");
INSERT INTO detail_pinjam VALUES("4","16","1","dikembalikan","50");
INSERT INTO detail_pinjam VALUES("5","14","1","dikembalikan","51");
INSERT INTO detail_pinjam VALUES("6","13","1","dipinjam","54");
INSERT INTO detail_pinjam VALUES("7","17","1","dikembalikan","55");
INSERT INTO detail_pinjam VALUES("8","18","1","dikembalikan","56");
INSERT INTO detail_pinjam VALUES("9","17","1","dikembalikan","57");
INSERT INTO detail_pinjam VALUES("10","15","1","dikembalikan","57");
INSERT INTO detail_pinjam VALUES("11","15","1","dipinjam","58");
INSERT INTO detail_pinjam VALUES("12","18","1","dipinjam","59");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kondisi` enum('Baru','Baik','Rusak') NOT NULL,
  `spesifikasi` text NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` int(5) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(5) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `sumber` text NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_petugas` (`id_petugas`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_jenis` (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("1","laptop4","Baru","Intel icore i3","oke","2","6","2019-03-14","2","V0026","1","SMKN1CIOMAS");
INSERT INTO inventaris VALUES("13","Tang","Baru","tang bla bla bla","baik","0","7","2019-03-21","1","V0018","1","SMKN1CIOMAS");
INSERT INTO inventaris VALUES("14","Laptop 99","Baru","Intel icore i3","ada","2","6","2019-03-19","1","V0025","1","SMKN1CIOMAS");
INSERT INTO inventaris VALUES("15","Kamera","Baru","Nikon bla bla bla","Ada","0","8","2019-03-18","3","V0021","1","SMKN1CIOMAS");
INSERT INTO inventaris VALUES("16","lenovo 60","Baru","Baru","ada","149","6","2019-02-21","5","V0001","1","SMKN1CIOMAS");
INSERT INTO inventaris VALUES("17","Tab Gambar","Baru","Pad bla bla bla","baik","1","6","2019-03-20","5","V0020","1","SMKN1CIOMAS");
INSERT INTO inventaris VALUES("18","Kaca mata las","Baru","las bla bla bla","baik","0","9","2019-03-19","4","V0019","1","SMKN1CIOMAS");
INSERT INTO inventaris VALUES("19","Laptop  03","Baik","Baik","baik","1","6","2019-02-25","2","V0004","2","SMKN1CIOMAS");
INSERT INTO inventaris VALUES("20","Laptop 02 ","Baru","Intel icore i3","ada","1","6","2019-03-22","1","V0023","1","SMKN1CIOMAS");
INSERT INTO inventaris VALUES("21","Laptop 01 ","Baru","Intel icore i3","Dipinjam","1","6","2019-03-25","1","V0030","1","SMKN1CIOMAS");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(100) NOT NULL,
  `kode_jenis` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("1","Laptop","16868","barang-barang elektronik");
INSERT INTO jenis VALUES("2","Proyektor","1890","sarana pembelajaran");
INSERT INTO jenis VALUES("3","Printer","1145","untuk ngeprint");
INSERT INTO jenis VALUES("4","Scanner","1126","untuk scan");
INSERT INTO jenis VALUES("5","Peralatan Audio, Video, dan Kelengkapannya","66789","Banyak banget");
INSERT INTO jenis VALUES("6","RPL","J0077","bagus");
INSERT INTO jenis VALUES("7","TKR","J0076","Jurusan TKR");
INSERT INTO jenis VALUES("8","BC","J0075","Jurusan BC");
INSERT INTO jenis VALUES("9","TPL","J0074","Jurusan TPL");
INSERT INTO jenis VALUES("10","ANM","J0073","Jurusan ANM");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(20) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");
INSERT INTO level VALUES("2","operator");



DROP TABLE pegawai;

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` int(15) NOT NULL,
  `alamat` varchar(80) NOT NULL,
  `no_telfon` int(15) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO pegawai VALUES("1","Nadia Mulyani","11972392","jl. Raya Laladon","1265738","nadia","nadia");
INSERT INTO pegawai VALUES("2","Bintang Yusuf","93764527","Jalan Laladon","81293103","bintang","bintang");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` enum('dipinjam','dikembalikan') NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_petugas` (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("50","2019-04-01","2019-04-30","dikembalikan","1");
INSERT INTO peminjaman VALUES("51","2019-04-01","2019-04-13","dikembalikan","1");
INSERT INTO peminjaman VALUES("55","2019-04-01","2019-04-17","dikembalikan","1");
INSERT INTO peminjaman VALUES("56","2019-04-01","2019-04-18","dikembalikan","2");
INSERT INTO peminjaman VALUES("57","2019-04-01","2019-04-18","dikembalikan","2");
INSERT INTO peminjaman VALUES("58","2019-04-01","2019-04-11","dipinjam","2");
INSERT INTO peminjaman VALUES("59","2019-04-02","2019-04-16","dipinjam","1");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `email` varchar(35) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  `baned` enum('No','Yes') NOT NULL,
  `logintime` int(2) DEFAULT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_lavel` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","admin","imanelvanhaz@gmail.com","admin","Nadia Mulyani","1","No","0");
INSERT INTO petugas VALUES("2","operator","nadiamulyani1423@gmail.com","operator","Nadia Mulyani","2","No","0");
INSERT INTO petugas VALUES("3","peminjam","nadiamulyani1423@gmail.com","peminjam","Nadia Mulyani","3","No","0");
INSERT INTO petugas VALUES("4","","","bintang2112","Bintang Yusuf","4","No","0");
INSERT INTO petugas VALUES("5","","","bintang2112","Bintang Yusuf","4","No","0");
INSERT INTO petugas VALUES("6","sdgi","ade@gmail.com","bintang2112","Bintang Yusuf","4","No","0");
INSERT INTO petugas VALUES("7","sdgi","ade@gmail.com","bintang2112","Bintang Yusuf","4","No","0");
INSERT INTO petugas VALUES("8","bintangyusuf","bintang@gmail.com","bintang2112","Bintang Yusuf","4","No","0");
INSERT INTO petugas VALUES("9","bintangyusuf","bintang@gmail.com","bintang2112","Bintang Yusuf","4","No","0");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(25) NOT NULL,
  `jenis_ruang` varchar(25) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("1","Lab RPL 1","Laboratorium","Kondisi lab bersih");
INSERT INTO ruang VALUES("2","kelas 12 rpl 2","ruang kelas belajar","baik");
INSERT INTO ruang VALUES("3","kelas 12 rpl 1","ruang kelas belajar","baik");
INSERT INTO ruang VALUES("4","kelas 12 rpl 3","ruang kelas belajar","baik");
INSERT INTO ruang VALUES("5","kelas 12 anm 1","ruang kelas belajar","baik");



