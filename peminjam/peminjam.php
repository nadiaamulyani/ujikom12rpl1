<?php
    include "header.php";
?>
              
<!-- start: Content -->
<div id="content">
  <div class="panel box-shadow-none content-header">
    <div class="panel-body">
      <div class="col-md-12">
        <h3 class="animated fadeInLeft">Inventaris</h3>
        <p class="animated fadeInDown">
          Inventaris <span class="fa-angle-right fa"></span> Tambah Data Jenis
        </p>
      </div>
    </div>
  </div>
			  
<div class="col-md-12" padding="0">
  <div class="col-md-12">
  <?php
    $jurusan = $_GET['jurusan'];
  ?>
    <form method="post" action="proses_tambah_peminjam.php?jurusan=<?= $jurusan ?>">
      <div class="form-element">
        <div class="panel form-element-padding">
					<div class="panel-heading"><h3>Tanggal Pengembalian <?= $jurusan ?></h3>
						<p>Ayo klo kamu mau minjem isi tanggal kembali dulu ya!!</p>
					</div>
          <div class="panel-body" style="padding-bottom:30px;">
          <div class="col-md-12">
					  
          <div class="form-group">
          <label>Tanggal Pengembalian</label>
            <input class="form-control" name="tanggal_kembali" type="date" required>
          </div>
						
        </div>
          <button type="submit" name="submit" class="btn btn-3d btn-primary" value="submit" >Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
</div> 
</div>
</div>

<?php
    include "footer.php";
?>