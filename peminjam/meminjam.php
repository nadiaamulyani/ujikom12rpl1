<?php
    include "header.php";
?>
              
<!-- start: Content -->
          <div id="content">
              <div class="panel box-shadow-none content-header">
                <div class="panel-body">
                  <div class="col-md-12">
                      <h3 class="animated fadeInLeft">Inventaris</h3>
                      <p class="animated fadeInDown">
                        Inventaris <span class="fa-angle-right fa"></span> Tambah Data Jenis
                      </p>
                  </div>
                </div>
              </div>
			  
<div class="col-md-12" padding="0">
  <div class="col-md-6">
    <form method="post" action="proses_tambah_meminjam.php?jurusan=<?= $_GET['jurusan'] ?>">
      <div class="form-element">
        <div class="panel form-element-padding">
					<div class="panel-heading"><h3>Peminjaman Barang</h3>
						<p>Nah sekarang kami bisa pilih barang dan jumlah yang akan dipijam</p>
					</div>
            <div class="panel-body" style="padding-bottom:30px;">
              <div class="col-md-12">

                <div class="form-group">
                  <label class="col-sm-2 control-label text-left">Barang</label>
                    <select class="form-control" name="id_inventaris">
                      <option value="" disabled seleted>Pilih Nama Barang</option>
                        <?php
                          include "../koneksi.php";
                          $query = mysqli_query($conn, "SELECT * FROM inventaris");
                          while($data=mysqli_fetch_array($query)) {
                        ?>
                      <option value="<?php echo $data['id_inventaris']; ?>"><?php echo $data['nama']; ?></option>
                        <?php } ?>
                    </select>
                </div>
					  
                <div class="form-group">
                  <label for="recipient-name" class="control-label">Jumlah</label>
                  <input name="jumlahp" type="text" class="form-control" value="1" readonly>
                </div>
						
						    <div class="form-group">
							    <label>Id Peminjam</label>
								    <?php
									    $id_peminjaman = $_GET['id_peminjaman'];
									    include "../koneksi.php";
									    $select = mysqli_query($conn, "SELECT * FROM peminjaman WHERE id_peminjaman = '$id_peminjaman'");
									    while($show = mysqli_fetch_array($select)){
								    ?>
								  <input name="id_peminjaman" type="number" value="<?php echo $show['id_peminjaman'] ?>" class="from-control" require="" readonly>
								    <?php
									    }
								    ?>
						    </div>
						
                </div>
                  <button type="submit" name="submit" class="btn btn-3d btn-primary" value="submit" >Simpan</button>
                </div>
              </form>
            </div>
          </div>
			</div>
			
  <div class="col-md-6">
    <div class="panel">
      <div class="panel-heading"><h3>Data Jenis</h3></div>
      <div class="panel-body">
        <div class="responsive-table">
        <table id="datatables-example" class="table table-striped table-bordered" cellspacing="0">
        <thead>
          <tr>
            <th>#</th>
            <th>Nama Barang</th>
            <th>Jumlah</th>
            <th>Jurusan</th>
          </tr>
        </thead>
        <tbody>
<?php // Load file koneksi.php
  include "../koneksi.php";
    if (isset($_GET['jurusan'])){
      $bebas = $_GET['jurusan'];
      $query = "SELECT * FROM inventaris INNER JOIN jenis ON inventaris.id_jenis = jenis.id_jenis WHERE jenis.nama_jenis = '$bebas'order by id_inventaris desc"; // Query untuk menampilkan semua data siswa
    } else {
      $query =  "SELECT * FROM inventaris INNER JOIN jenis ON inventaris.id_jenis = jenis.id_jenis order by id_inventaris desc";          
    }
      $sql = mysqli_query($conn, $query); // Eksekusi/Jalankan query dari variabel $query
      $no=1;
    while($data = mysqli_fetch_array($sql)){
?> 
          <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo $data['nama']; ?></td>
            <td><?php echo $data['jumlah']; ?></td>
            <td><?php echo $data['nama_jenis']; ?></td>
          </tr>
          <?php } ?>
        </tbody>
          </table>
          </div> 
          </div> 
    </div>
  </div>
</div> 
</div>
</div>

<?php
    include "footer.php";
?>