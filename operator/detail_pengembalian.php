<?php
  include "header.php";
?>
 <!-- start: Content -->
 <div id="content">
 <div class="panel box-shadow-none content-header">
    <div class="panel-body">
      <div class="col-md-12">
          <h3 class="animated fadeInLeft">Data Peminjaman</h3>
          <p class="animated fadeInDown">
            Peminjaman <span class="fa-angle-right fa"></span> Data Detail Pinjam
          </p>
      </div>
    </div> 
    </div>
<div class="col-md-12 top-20 padding-0">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading"><h3>Data Detail Pinjam</h3></div>
      <div class="panel-body">
        <div class="responsive-table">
        <table id="datatables-example" class="table table-striped table-bordered" cellspacing="0">
        <thead>
          <tr>
            <th>#</th>
            <th>Id Invetaris</th>
            <th>Jumlah</th>
            <th>Status Peminjaman</th>
            <th>Id Peminjaman</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
<?php // Load file koneksi.php
  include "../koneksi.php";
  $id_peminjaman = $_GET['id_peminjaman'];
  $query = "SELECT * FROM detail_pinjam JOIN inventaris on inventaris.id_inventaris = detail_pinjam.id_inventaris where detail_pinjam.id_peminjaman='$id_peminjaman' ";
  $sql = mysqli_query($conn, $query); // Eksekusi/Jalankan query dari variabel $query
  $no=1;
  while($data = mysqli_fetch_array($sql)){
?>
          <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo $data['nama']; ?></td>
            <td><?php echo $data['jumlahp']; ?></td>
            <td><?php echo $data['status_peminjaman']; ?></td>
            <td><?php echo $_GET ['id_peminjaman']; ?></td>
            <td>
            <?php if ($data['status_peminjaman'] == 'dipinjam') { ?>
            <form action="proses_pengembalian.php" method="post">
                  <input type="hidden" name="id_peminjaman" value="<?php echo $_GET['id_peminjaman']?>">
                  <button type="submit">Dipinjam</button>
            </form>
            <?php }else{ ?>
            <input type="hidden" name="id_peminjaman" value="<?php echo $_GET['id_peminjaman']?>">
                  <button type="submit">Dikembalikan</button>
            <?php } ?>
            </form>
            </td>
          </tr>
          <?php } ?>
        </tbody>
          </table>
          </div>
          </div> 
    </div>
  </div>
</div>  
</div>
</div>
<!-- end: content -->

<?php
  include "footer.php";
?>