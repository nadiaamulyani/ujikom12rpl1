<?php
  include "header.php";
?>
 <!-- start: Content -->
 <div id="content">
 <div class="panel box-shadow-none content-header">
    <div class="panel-body">
      <div class="col-md-12">
          <h3 class="animated fadeInLeft">Data Peminjam</h3>
          <p class="animated fadeInDown">
            Operator <span class="fa-angle-right fa"></span> Data Peminjam
          </p>
      </div>
    </div> 
    </div>
<div class="col-md-12 top-20 padding-0">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading"><h3>Data Peminjam</h3></div>
      <div class="panel-body">
        <div class="responsive-table">
        <table id="datatables-example" class="table table-striped table-bordered" cellspacing="0">
        <thead>
          <tr>
            <th>#</th>
            <th>Nama Peminjam</th>
            <th>Tanggal Pinjam</th>
            <th>Tanggal Kembali</th>
            <th>Status Peminjam</th>
			<th>Detail</th>
          </tr>
        </thead>
        <tbody>
<?php // Load file koneksi.php
  include "../koneksi.php";

  $query = "SELECT * FROM peminjaman INNER JOIN pegawai ON peminjaman.id_pegawai = pegawai.id_pegawai order by id_peminjaman desc"; // Query untuk menampilkan semua data siswa
  $sql = mysqli_query($conn, $query); // Eksekusi/Jalankan query dari variabel $query
  $no=1;
  while($data = mysqli_fetch_array($sql)){
?>

          <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo $data['nama_pegawai']; ?></td>
            <td><?php echo $data['tanggal_pinjam']; ?></td>
            <td><?php echo $data['tanggal_kembali']; ?></td>
            <td><?php echo $data['status_peminjaman']; ?></td>
            <td>
          <div class="col-md-6">
              <a href="detail_peminjam.php?id_peminjaman=<?php echo $data['id_peminjaman']; ?>" type="button" class="btn btn-3d btn-success">View</a>
          </div>
            </td>
          </tr>
          <?php } ?>
        </tbody>
          </table>
          </div>
          </div> 
    </div>
  </div>
</div>  
</div>
</div>
<!-- end: content -->

<?php
  include "footer.php";
?>