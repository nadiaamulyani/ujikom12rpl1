-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 06 Apr 2019 pada 09.13
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujikom`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlahp` varchar(5) NOT NULL,
  `status_peminjaman` enum('dipinjam','dikembalikan') NOT NULL,
  `id_peminjaman` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_pinjam`, `id_inventaris`, `jumlahp`, `status_peminjaman`, `id_peminjaman`) VALUES
(1, 1, '1', 'dikembalikan', 1),
(2, 1, '1', 'dikembalikan', 2),
(3, 16, '1', 'dipinjam', 4),
(4, 1, '1', 'dipinjam', 4),
(5, 19, '1', 'dipinjam', 5);

--
-- Trigger `detail_pinjam`
--
DELIMITER $$
CREATE TRIGGER `kembali` BEFORE UPDATE ON `detail_pinjam` FOR EACH ROW UPDATE inventaris SET jumlah = jumlah+NEW.jumlahp WHERE id_inventaris = new.id_inventaris
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `pengurangan_stok` AFTER INSERT ON `detail_pinjam` FOR EACH ROW UPDATE inventaris SET jumlah = jumlah - NEW.jumlahp WHERE id_inventaris = new.id_inventaris
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kondisi` enum('Baru','Baik','Rusak') NOT NULL,
  `spesifikasi` text NOT NULL,
  `keterangan_inventaris` enum('Tersedia','Dipinjam') NOT NULL,
  `jumlah` int(5) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(5) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `sumber` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `spesifikasi`, `keterangan_inventaris`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_petugas`, `sumber`) VALUES
(1, 'laptop4', 'Baru', 'Intel icore i3', 'Tersedia', 3, 6, '2019-03-14', 2, 'V0026', 1, 'SMKN1CIOMAS'),
(13, 'Tang', 'Baru', 'tang bla bla bla', 'Tersedia', 0, 7, '2019-03-21', 1, 'V0018', 1, 'SMKN1CIOMAS'),
(14, 'Laptop 99', 'Baru', 'Intel icore i3', 'Tersedia', 0, 6, '2019-03-19', 1, 'V0025', 1, 'SMKN1CIOMAS'),
(15, 'Kamera', 'Baru', 'Nikon bla bla bla', 'Tersedia', 0, 8, '2019-03-18', 3, 'V0021', 1, 'SMKN1CIOMAS'),
(16, 'lenovo 60', 'Baru', 'Baru', 'Tersedia', 0, 6, '2019-02-21', 5, 'V0001', 1, 'SMKN1CIOMAS'),
(17, 'Tab Gambar', 'Baru', 'Pad bla bla bla', 'Tersedia', 0, 10, '2019-03-20', 5, 'V0020', 1, 'SMKN1CIOMAS'),
(18, 'Kaca mata las', 'Baru', 'las bla bla bla', 'Tersedia', 1, 9, '2019-03-19', 4, 'V0019', 1, 'SMKN1CIOMAS'),
(19, 'Laptop  03', 'Baik', 'Baik', 'Tersedia', 0, 6, '2019-02-25', 2, 'V0004', 2, 'SMKN1CIOMAS'),
(20, 'Laptop 02 ', 'Baru', 'Intel icore i3', 'Tersedia', 1, 1, '2019-04-19', 1, 'V0031', 1, 'smk negeri 1 ciomas'),
(21, 'Laptop 01 ', 'Baru', 'Intel icore i3', 'Tersedia', 1, 6, '2019-03-25', 1, 'V0030', 1, 'SMKN1CIOMAS'),
(22, 'laptop12', 'Baik', 'Intel icore i3', 'Tersedia', 1, 6, '2019-04-05', 3, 'V0032', 2, 'smk negeri 1 ciomas'),
(23, 'laptop13', 'Baik', 'Intel icore i3', 'Tersedia', 1, 6, '2019-04-05', 4, 'V0033', 3, 'smk negeri 1 ciomas'),
(24, 'infocus 01', 'Baru', 'oks12w', 'Tersedia', 1, 6, '2019-04-05', 3, 'V0034', 3, 'smk negeri 1 ciomas'),
(25, 'komputer', 'Baru', 'oke mantap', 'Tersedia', 1, 10, '2019-04-06', 5, 'V0035', 3, 'smk negeri 1 ciomas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL,
  `kode_jenis` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(1, 'Laptop', '16868', 'barang-barang elektronik'),
(2, 'Proyektor', '1890', 'sarana pembelajaran'),
(3, 'Printer', '1145', 'untuk ngeprint'),
(4, 'Scanner', '1126', 'untuk scan'),
(5, 'Peralatan Audio, Video, dan Kelengkapannya', '66789', 'Banyak banget'),
(6, 'RPL', 'J0077', 'Jurusan RPL'),
(7, 'TKR', 'J0076', 'Jurusan TKR'),
(8, 'BC', 'J0075', 'Jurusan BC'),
(9, 'TPL', 'J0074', 'Jurusan TPL'),
(10, 'ANM', 'J0073', 'Jurusan ANM');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'operator'),
(3, 'peminjam');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` int(15) NOT NULL,
  `alamat` varchar(80) NOT NULL,
  `no_telfon` int(15) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`, `no_telfon`, `username`, `password`) VALUES
(1, 'Nadia Mulyani', 11972392, 'jl. Raya Laladon', 1265738, 'nadia', 'nadia'),
(2, 'Bintang Yusuf', 93764527, 'Jalan Laladon', 81293103, 'bintang', 'bintang');

-- --------------------------------------------------------

--
-- Stand-in structure for view `peminjam`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `peminjam` (
`id_inventaris` int(11)
,`id_peminjaman` int(11)
,`nama_pegawai` varchar(50)
,`nama` varchar(50)
,`nama_ruang` varchar(25)
,`jumlah` varchar(5)
,`status_peminjaman` enum('dipinjam','dikembalikan')
,`tanggal_pinjam` date
,`tanggal_kembali` date
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` enum('dipinjam','dikembalikan') NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_pegawai`) VALUES
(1, '2019-04-05', '2019-04-18', 'dikembalikan', 1),
(2, '2019-04-05', '2019-04-16', 'dikembalikan', 1),
(4, '2019-04-05', '2019-04-12', 'dipinjam', 1),
(5, '2019-04-06', '2019-04-25', 'dipinjam', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `email` varchar(35) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  `baned` enum('No','Yes') NOT NULL,
  `logintime` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `email`, `password`, `nama_petugas`, `id_level`, `baned`, `logintime`) VALUES
(1, 'admin', 'imanelvanhaz@gmail.com', 'jaBTd51b', 'Nadia Mulyani', 1, 'No', 0),
(2, 'operator', 'nadiamulyani1423@gmail.com', 'operator', 'nadiaamulyani', 2, 'No', 0),
(3, 'peminjam', 'nadiamulyani1423@gmail.com', 'peminjam', 'nananadia', 3, 'No', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `recovery_keys`
--

CREATE TABLE `recovery_keys` (
  `rid` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `token` varchar(50) NOT NULL,
  `valid` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(25) NOT NULL,
  `jenis_ruang` varchar(25) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `jenis_ruang`, `keterangan`) VALUES
(1, 'Lab RPL 1', 'Laboratorium', 'Kondisi lab bersih'),
(2, 'kelas 12 rpl 2', 'ruang kelas belajar', 'baik'),
(3, 'kelas 12 rpl 1', 'ruang kelas belajar', 'baik'),
(4, 'kelas 12 rpl 3', 'ruang kelas belajar', 'baik'),
(5, 'kelas 12 anm 1', 'ruang kelas belajar', 'baik');

-- --------------------------------------------------------

--
-- Struktur untuk view `peminjam`
--
DROP TABLE IF EXISTS `peminjam`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `peminjam`  AS  select `c`.`id_inventaris` AS `id_inventaris`,`d`.`id_peminjaman` AS `id_peminjaman`,`a`.`nama_pegawai` AS `nama_pegawai`,`c`.`nama` AS `nama`,`e`.`nama_ruang` AS `nama_ruang`,`b`.`jumlahp` AS `jumlah`,`d`.`status_peminjaman` AS `status_peminjaman`,`d`.`tanggal_pinjam` AS `tanggal_pinjam`,`d`.`tanggal_kembali` AS `tanggal_kembali` from ((((`pegawai` `a` join `detail_pinjam` `b`) join `inventaris` `c`) join `peminjaman` `d`) join `ruang` `e`) where ((`e`.`id_ruang` = `c`.`id_ruang`) and (`a`.`id_pegawai` = `d`.`id_pegawai`) and (`b`.`id_inventaris` = `c`.`id_inventaris`) and (`b`.`id_peminjaman` = `d`.`id_peminjaman`)) order by `d`.`id_peminjaman` desc ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id_detail_pinjam`),
  ADD KEY `id_inventaris` (`id_inventaris`),
  ADD KEY `id_peminjaman` (`id_peminjaman`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`),
  ADD KEY `id_petugas` (`id_petugas`),
  ADD KEY `id_ruang` (`id_ruang`),
  ADD KEY `id_jenis` (`id_jenis`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`),
  ADD KEY `id_petugas` (`id_pegawai`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`),
  ADD KEY `id_lavel` (`id_level`);

--
-- Indexes for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
  ADD PRIMARY KEY (`rid`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
  MODIFY `rid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
